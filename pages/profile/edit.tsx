import Container from "../../components/Container";
import { useUserData } from "../../lib/hooks";
import { updateUserProfile, createUserProfile } from "../../lib/db/users";
import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import Metatags from "../../components/MetaTags";

export default function EditProfile() {
  const { user, userProfile } = useUserData();
  const router = useRouter();

  const [firstName, setFirstName] = useState(userProfile?.firstName || "");
  const [lastName, setLastName] = useState(userProfile?.lastName || "");
  const [bio, setBio] = useState(userProfile?.bio || "");
  const [school, setSchool] = useState(userProfile?.school || "");
  const [major, setMajor] = useState(userProfile?.major || "");
  const [gpa, setGpa] = useState(userProfile?.gpa || "");

  useEffect(() => {
    if (!userProfile) {
    }
  }),
    [user];

  const handleSubmit = (e) => {
    e.preventDefault();
    if (userProfile) {
      console.log(userProfile.userId);
      updateUserProfile(userProfile.id, {
        ...userProfile,
        firstName,
        lastName,
        bio,
        school,
        major,
        gpa,
      });
    } else {
      const newProfile = {
        userId: user.uid,
        firstName,
        lastName,
        bio,
        school,
        major,
        gpa,
      };
      createUserProfile(newProfile);
      router.push("/profile");
    }
  };

  return (
    <>
      <Metatags
        title="Scholly | Edit Profile"
        description="Edit Main Profile information."
      />
      <Container>
        <div className="mx-auto mt-8 grid max-w-3xl grid-cols-1 gap-6 sm:px-6 lg:max-w-7xl lg:grid-flow-col-dense">
          <div className="space-y-6 lg:col-span-2 lg:col-start-1">
            <form onSubmit={handleSubmit}>
              <div className="shadow sm:overflow-hidden sm:rounded-md">
                <div className="space-y-6 bg-white py-6 px-4 sm:p-6">
                  <div>
                    <h3 className="text-lg font-medium leading-6 text-gray-900">
                      Profile
                    </h3>
                    <p className="mt-1 text-sm text-gray-500">
                      This information will be displayed publicly so be careful
                      what you share.
                    </p>
                  </div>

                  <div className="grid grid-cols-3 gap-6">
                    <div className="col-span-3">
                      <label
                        htmlFor="about"
                        className="block text-sm font-medium text-gray-700"
                      >
                        About
                      </label>
                      <div className="mt-1">
                        <textarea
                          id="about"
                          name="about"
                          rows={3}
                          className="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                          placeholder="you@example.com"
                          defaultValue={bio}
                          onChange={(e) => setBio(e.target.value)}
                        />
                      </div>
                      <p className="mt-2 text-sm text-gray-500">
                        {userProfile
                          ? userProfile.bio
                          : "Brief description for your profile. URLs are hyperlinked."}
                      </p>
                    </div>

                    <div className="col-span-6 sm:col-span-3">
                      <label
                        htmlFor="first-name"
                        className="block text-sm font-medium text-gray-700"
                      >
                        First name
                      </label>
                      <input
                        type="text"
                        name="first-name"
                        id="first-name"
                        autoComplete="given-name"
                        value={firstName}
                        onChange={(e) => setFirstName(e.target.value)}
                        className="mt-1 block w-full rounded-md border border-gray-300 py-2 px-3 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                      />
                    </div>

                    <div className="col-span-6 sm:col-span-3">
                      <label
                        htmlFor="last-name"
                        className="block text-sm font-medium text-gray-700"
                      >
                        Last name
                      </label>
                      <input
                        type="text"
                        name="last-name"
                        id="last-name"
                        autoComplete="family-name"
                        value={lastName}
                        onChange={(e) => setLastName(e.target.value)}
                        className="mt-1 block w-full rounded-md border border-gray-300 py-2 px-3 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                      />
                    </div>

                    <div className="sm:col-span-1">
                      <label
                        htmlFor="school"
                        className="block text-sm font-medium text-gray-700"
                      >
                        School
                      </label>
                      <input
                        type="text"
                        name="school"
                        id="school"
                        autoComplete="school-name"
                        value={school}
                        onChange={(e) => setSchool(e.target.value)}
                        className="mt-1 block w-full rounded-md border border-gray-300 py-2 px-3 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                      />
                    </div>

                    <div className="sm:col-span-1">
                      <label
                        htmlFor="major"
                        className="block text-sm font-medium text-gray-700"
                      >
                        Major
                      </label>
                      <input
                        type="text"
                        name="major"
                        id="major"
                        autoComplete="major-name"
                        value={major}
                        onChange={(e) => setMajor(e.target.value)}
                        className="mt-1 block w-full rounded-md border border-gray-300 py-2 px-3 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                      />
                    </div>

                    <div className="sm:col-span-1">
                      <label
                        htmlFor="gpa"
                        className="block text-sm font-medium text-gray-700"
                      >
                        GPA
                      </label>
                      <input
                        type="text"
                        name="gpa"
                        id="gpa"
                        autoComplete="gpa-score"
                        value={gpa}
                        onChange={(e) => setGpa(e.target.value)}
                        className="mt-1 block w-full rounded-md border border-gray-300 py-2 px-3 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                      />
                    </div>

                    <div className="col-span-3">
                      <label className="block text-sm font-medium text-gray-700">
                        Resume
                      </label>
                      <div className="mt-1 flex justify-center rounded-md border-2 border-dashed border-gray-300 px-6 pt-5 pb-6">
                        <div className="space-y-1 text-center">
                          <svg
                            className="mx-auto h-12 w-12 text-gray-400"
                            stroke="currentColor"
                            fill="none"
                            viewBox="0 0 48 48"
                            aria-hidden="true"
                          >
                            <path
                              d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02"
                              strokeWidth={2}
                              strokeLinecap="round"
                              strokeLinejoin="round"
                            />
                          </svg>
                          <div className="flex text-sm text-gray-600">
                            <label
                              htmlFor="file-upload"
                              className="relative cursor-pointer rounded-md bg-white font-medium text-indigo-600 focus-within:outline-none focus-within:ring-2 focus-within:ring-indigo-500 focus-within:ring-offset-2 hover:text-indigo-500"
                            >
                              <span>Upload a file</span>
                              <input
                                id="file-upload"
                                name="file-upload"
                                type="file"
                                className="sr-only"
                              />
                            </label>
                            <p className="pl-1">or drag and drop</p>
                          </div>
                          <p className="text-xs text-gray-500">
                            PNG, JPG, GIF up to 10MB
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="bg-gray-50 px-4 py-3 text-right sm:px-6">
                  <button
                    type="submit"
                    className="inline-flex justify-center rounded-md border border-transparent bg-indigo-600 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                  >
                    Save
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </Container>
    </>
  );
}
