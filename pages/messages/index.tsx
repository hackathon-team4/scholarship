import Container from "../../components/Container";
import Header from "../../components/header";
import Metatags from "../../components/MetaTags";

const people = [
  {
    name: "Lindsay Walton",
    imageUrl:
      "https://images.unsplash.com/photo-1517841905240-472988babdf9?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=3&w=256&h=256&q=80",
    message: "I'm interested in sponsoring your studies. Let's talk!",
  },
  {
    name: "Michael Scott",
    imageUrl:
      "https://images.unsplash.com/photo-1463453091185-61582044d556?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTF8fHByb2ZpbGUlMjBwaWNzfGVufDB8fDB8fA%3D%3D&auto=format&fit=facearea&facepad=3&w=256&h=256&q=80",
    message: "It was great talking! I hope we can stay in touch.",
  },
  {
    name: "Lebron James",
    imageUrl:
      "https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTh8fHByb2ZpbGUlMjBwaWNzfGVufDB8fDB8fA%3D%3D&auto=format&fit=facearea&facepad=3&w=256&h=256&q=80",
    message: "Tell me more about your goals. I'm here to help!",
  },
  {
    name: "Serena Williams",
    imageUrl:
      "https://images.unsplash.com/photo-1522307837370-cc113a36b784?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTV8fHByb2ZpbGUlMjBwaWNzfGVufDB8fDB8fA%3D%3D&auto=format&fit=facearea&facepad=3&w=256&h=256&q=80",
    message: "Where do you see yourself in 5 years?",
  },
  {
    name: "Bill Gates",
    imageUrl:
      "https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTN8fHByb2ZpbGUlMjBwaWNzfGVufDB8fDB8fA%3D%3D&auto=format&fit=facearea&facepad=3&w=256&h=256&q=80",
    message:
      "It's better to shoot for the moon and miss than to not shoot at all.",
  },
];
const activityItems = [
  {
    id: 1,
    person: people[0],
    project: "Workcation",
    commit: "2d89f0c8",
    environment: "production",
    time: "1h",
  },
  {
    id: 2,
    person: people[1],
    project: "Workcation",
    commit: "2d89f0c8",
    environment: "production",
    time: "3h",
  },
  {
    id: 3,
    person: people[2],
    project: "Workcation",
    commit: "2d89f0c8",
    environment: "production",
    time: "9h",
  },
  {
    id: 4,
    person: people[3],
    project: "Workcation",
    commit: "2d89f0c8",
    environment: "production",
    time: "1d",
  },
  {
    id: 5,
    person: people[4],
    project: "Workcation",
    commit: "2d89f0c8",
    environment: "production",
    time: "5d",
  },
  // More items...
];

export default function Messages() {
  return (
    <>
      <Metatags
        title="Sabio | Messages"
        description="Get in contact with potential sponsors through this integrated messaging system."
      />

      <Container>
        <Header title="Messages" description="Manage messages here" />
        <div>
          <ul role="list" className="px-20 divide-y divide-gray-200">
            {activityItems.map((activityItem) => (
              <li key={activityItem.id} className="py-4">
                <div className="flex space-x-3">
                  <img
                    className="h-6 w-6 rounded-full"
                    src={activityItem.person.imageUrl}
                    alt=""
                  />
                  <div className="flex-1 space-y-1">
                    <div className="flex items-center justify-between">
                      <h3 className="text-sm font-medium">
                        {activityItem.person.name}
                      </h3>
                      <p className="text-sm text-gray-500">
                        {activityItem.time}
                      </p>
                    </div>
                    <p className="text-sm text-gray-500">
                      {activityItem.person.message}
                    </p>
                  </div>
                </div>
              </li>
            ))}
          </ul>
        </div>
      </Container>
    </>
  );
}
