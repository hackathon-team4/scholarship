import { signInWithGoogle } from "../lib/db/users";
import { useRouter } from "next/router";
import { useState } from "react";
import { RadioGroup } from "@headlessui/react";
import { CheckCircleIcon } from "@heroicons/react/20/solid";
import { db } from "../lib/firebase";
import Metatags from "../components/MetaTags";

const options = [
  { id: 1, name: "Student" },
  { id: 2, name: "Sponsor" },
];

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}
function OptionsComp() {
  const [selectedRole, setSelectedRole] = useState();

  return (
    <RadioGroup value={selectedRole} onChange={(e) => setSelectedRole(e)}>
      <RadioGroup.Label className="text-base font-medium text-gray-900">
        Select a role
      </RadioGroup.Label>

      <div className="mt-4 grid grid-cols-1 gap-y-6 sm:grid-cols-3 sm:gap-x-4">
        {options.map((option) => (
          <RadioGroup.Option
            key={option.id}
            value={option.name}
            className={({ checked, active }) =>
              classNames(
                checked ? "border-transparent" : "border-gray-300",
                active ? "border-indigo-500 ring-2 ring-indigo-500" : "",
                "relative flex cursor-pointer rounded-lg border bg-white p-4 shadow-sm focus:outline-none"
              )
            }
          >
            {({ checked, active }) => (
              <>
                <span className="flex flex-1">
                  <span className="flex flex-col">
                    <RadioGroup.Label
                      as="span"
                      className="block text-sm font-medium text-gray-900"
                    >
                      {option.name}
                    </RadioGroup.Label>
                  </span>
                </span>
                <CheckCircleIcon
                  className={classNames(
                    !checked ? "invisible" : "",
                    "h-5 w-5 text-indigo-600"
                  )}
                  aria-hidden="true"
                />
                <span
                  className={classNames(
                    active ? "border" : "border-2",
                    checked ? "border-indigo-500" : "border-transparent",
                    "pointer-events-none absolute -inset-px rounded-lg"
                  )}
                  aria-hidden="true"
                />
              </>
            )}
          </RadioGroup.Option>
        ))}
      </div>
    </RadioGroup>
  );
}

export default function SignUp() {
  const router = useRouter();
  const [selectedRole, setSelectedRole] = useState(options[0]);
  const handleSignIn = async () => {
    const res = await signInWithGoogle();
    if (res.user) {
      db.collection("users").doc(res.user.uid).set({
        displayName: res.user.displayName,
        id: res.user.uid,
        role: selectedRole.name,
      });
    }

    router.push("/dashboard");
  };

  return (
    <>
      <Metatags
        title="Sabio | Sign Up"
        description="Sign up for an account to get started."
      />
      <div className="bg-gray-50">
        <div className="mx-auto max-w-7xl py-12 px-4 sm:px-6 lg:items-center lg:justify-between lg:py-16 lg:px-8">
          <h2 className="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">
            <span className="block">Ready to dive in?</span>
            <span className="block text-indigo-600">
              Select to be a Sponsor or Student and Sign in with Google.
            </span>
          </h2>
          <RadioGroup value={selectedRole} onChange={setSelectedRole}>
            <RadioGroup.Label className="text-base font-medium text-gray-900">
              Select a role
            </RadioGroup.Label>

            <div className="mt-4 grid grid-cols-1 gap-y-6 sm:grid-cols-3 sm:gap-x-4">
              {options.map((option) => (
                <RadioGroup.Option
                  key={option.id}
                  value={option}
                  className={({ checked, active }) =>
                    classNames(
                      checked ? "border-transparent" : "border-gray-300",
                      active ? "border-indigo-500 ring-2 ring-indigo-500" : "",
                      "relative flex cursor-pointer rounded-lg border bg-white p-4 shadow-sm focus:outline-none"
                    )
                  }
                >
                  {({ checked, active }) => (
                    <>
                      <span className="flex flex-1">
                        <span className="flex flex-col">
                          <RadioGroup.Label
                            as="span"
                            className="block text-sm font-medium text-gray-900"
                          >
                            {option.name}
                          </RadioGroup.Label>
                        </span>
                      </span>
                      <CheckCircleIcon
                        className={classNames(
                          !checked ? "invisible" : "",
                          "h-5 w-5 text-indigo-600"
                        )}
                        aria-hidden="true"
                      />
                      <span
                        className={classNames(
                          active ? "border" : "border-2",
                          checked ? "border-indigo-500" : "border-transparent",
                          "pointer-events-none absolute -inset-px rounded-lg"
                        )}
                        aria-hidden="true"
                      />
                    </>
                  )}
                </RadioGroup.Option>
              ))}
            </div>
          </RadioGroup>
          <div className="mt-8 flex lg:mt-9 lg:flex-shrink-0">
            <div className="rounded-md shadow">
              <button
                onClick={handleSignIn}
                className="inline-flex items-center justify-center rounded-md border border-transparent bg-indigo-600 px-5 py-3 text-base font-medium text-white hover:bg-indigo-700"
              >
                <img src="/google.svg" width="30px" className="mr-2"></img>
                Sign in with Google
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
