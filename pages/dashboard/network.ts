import {
  getApplicationsCount,
  getApprovedApplicationsCount,
  getApprovedApplicationsTotal,
} from "../../lib/db/applications";

export default async function getStats(userId: string) {
  const submitted = await getApplicationsCount(userId);
  const received = await getApprovedApplicationsCount(userId);
  const money = await getApprovedApplicationsTotal(userId);
  return [`${submitted}`, `${received}`, `${money}`];
}
