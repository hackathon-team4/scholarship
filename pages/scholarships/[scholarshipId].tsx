import { CheckCircleIcon } from "@heroicons/react/24/outline";
import { useRouter } from "next/router";
import { useState } from "react";
import Container from "../../components/Container";
import Metatags from "../../components/MetaTags";
import getScholarship from "./network";

export default function Scholarship() {
  const router = useRouter();
  const { scholarshipId, user } = router.query;
  const [scholarship, setScholarship] = useState(null);
  const [alreadyApplied, setAlreadyApplied] = useState(false);

  if (scholarshipId instanceof Array) {
    // error here
    return <div>error</div>;
  } else {
    getScholarship(scholarshipId).then((data) => {
      setScholarship(data);
    });
  }

  return (
    <>
      <Metatags
        title="Scholly | Scholarship"
        description="See the details of a specific scholarship"
      />
      <Container>
        <div className="py-10 px-4 sm:px-6 lg:px-8">
          <div className="py-10 px-4 sm:px-6 lg:px-8">
            <div className="sm:flex sm:items-center">
              <div className="sm:flex-auto">
                <h1 className="text-xl font-semibold text-gray-900">
                  Scholarship
                </h1>
              </div>
              <div className="mt-4 sm:mt-0 sm:ml-16 sm:flex-none">
                {!alreadyApplied ? (
                  <button
                    type="button"
                    onClick={() => {
                      setAlreadyApplied(true);
                    }}
                    className="inline-flex items-center justify-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:w-auto"
                  >
                    Apply
                  </button>
                ) : (
                  <CheckCircleIcon
                    className="h-8 w-8 text-green-400"
                    aria-hidden="true"
                  />
                )}
              </div>
            </div>
          </div>

          <div className="bg-white shadow overflow-hidden sm:rounded-lg">
            <div className="px-4 py-5 sm:px-6">
              <h3 className="text-lg leading-6 font-medium text-gray-900">
                Scholarship Information
              </h3>
              <p className="mt-1 max-w-2xl text-sm text-gray-500">
                Personal details and application.
              </p>
            </div>
            <div className="border-t border-gray-200">
              <dl>
                <div className="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                  <dt className="text-sm font-medium text-gray-500">Name</dt>
                  <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                    {scholarship?.name}
                  </dd>
                </div>
                <div className="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                  <dt className="text-sm font-medium text-gray-500">
                    Application Type
                  </dt>
                  <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                    {scholarship?.type}
                  </dd>
                </div>
                <div className="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                  <dt className="text-sm font-medium text-gray-500">
                    Scholarship Amount
                  </dt>
                  <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                    {scholarship?.value}
                  </dd>
                </div>
                <div className="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                  <dt className="text-sm font-medium text-gray-500">
                    Description
                  </dt>
                  <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                    {scholarship?.description}
                  </dd>
                </div>
              </dl>
            </div>
          </div>
        </div>
      </Container>
    </>
  );
}
