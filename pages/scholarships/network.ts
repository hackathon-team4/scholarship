import { getScholarshipsById } from "../../lib/db/scholarships";

export default async function getScholarship(id: string) {
  const scholarship = await getScholarshipsById(id);
  return scholarship;
}
