import { CheckCircleIcon } from "@heroicons/react/24/outline";
import { useState } from "react";
import Container from "../../components/Container";
import Metatags from "../../components/MetaTags";
import {
  getPaginatedScholarships,
  getScholarshipsById,
} from "../../lib/db/scholarships";

export async function getServerSideProps() {
  const scholarships = await getPaginatedScholarships();
  return {
    props: {
      scholarships,
    },
  };
}

export default function Scholarships({ scholarships }) {
  const [alreadyApplied, setAlreadyApplied] = useState(false);
  return (
    <>
      <Metatags
        title="Sabio | Scholarships"
        description="See all available scholarships. Scholarship search made easy."
      />
      <Container>
        <div className="relative bg-gray-50 px-4 pt-10 pb-20 sm:px-6 lg:px-8 lg:pb-28">
          <div className="absolute inset-0">
            <div className="h-1/3 bg-white sm:h-2/3" />
          </div>
          <div className="relative mx-auto max-w-7xl">
            <div className="text-center">
              <h2 className="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">
                Scholarship Opportunities
              </h2>
              <p className="mx-auto mt-3 max-w-2xl text-xl text-gray-500 sm:mt-4">
                Apply for scholarships to help make your college dreams a
                reality
              </p>
            </div>
            <div className="mx-auto mt-12 grid max-w-lg gap-5 lg:max-w-none lg:grid-cols-3">
              {scholarships.map((scholarship) => (
                // eslint-disable-next-line react/jsx-key
                <Card scholarship={scholarship} />
              ))}
            </div>
          </div>
        </div>
      </Container>
    </>
  );
}

function Card({ scholarship }) {
  const [alreadyApplied, setAlreadyApplied] = useState(false);
  return (
    <div
      key={scholarship.name}
      className="flex flex-col overflow-hidden rounded-lg shadow-lg"
    >
      <div className="flex-shrink-0">
        <img
          className="h-48 w-full object-cover"
          src={scholarship.image}
          alt=""
        />
      </div>
      <div className="flex flex-1 flex-col justify-between bg-white p-6">
        <div className="flex-1">
          <p className="text-sm font-medium text-indigo-600">
            <a href="#" className="hover:underline">
              {scholarship.type}
            </a>
          </p>
          <a href="#" className="mt-2 block">
            <p className="text-xl font-semibold text-gray-900">
              {scholarship.name}
            </p>
            <p className="mt-3 text-base text-gray-500">
              {scholarship.description}
            </p>
          </a>
        </div>
        <div className="mt-6 flex items-center">
          <div className="flex-shrink-0">
            <a href="#">
              <span className="sr-only">{scholarship.sponsorID}</span>
              <img
                className="h-10 w-10 rounded-full"
                src={scholarship.image}
                alt=""
              />
            </a>
          </div>
          <div className="ml-3">
            <p className="text-sm font-medium text-gray-900">
              <a href="#" className="sr-only">
                {scholarship.sponsorID}
              </a>
            </p>
            <div className="flex space-x-1 text-xs text-gray-500">
              <time dateTime={scholarship.deadline}>{scholarship.date}</time>
            </div>
          </div>
          <div className="ml-auto flex-shrink-0">
            <div className="pr-5">
              {!alreadyApplied ? (
                <button
                  type="button"
                  onClick={() => {
                    setAlreadyApplied(true);
                  }}
                  className="inline-flex items-center px-3 py-0.5 border border-transparent text-sm font-medium rounded-full shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                >
                  Apply
                </button>
              ) : (
                <CheckCircleIcon
                  className="h-8 w-8 text-green-400"
                  aria-hidden="true"
                />
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
