import { db, objectToJSON } from "../firebase";
import { Scholarship } from "../models";

export const getPaginatedScholarships = async () => {
  const query = db
    .collectionGroup("scholarships")

  return (await query.get()).docs.map(objectToJSON);
};

export const getScholarshipsByUser = async (userID: string) => {
  const query = db
    .collection("users")
    .doc(userID)
    .collection("scholarships")
    .orderBy("createdAt", "desc");

  return (await query.get()).docs.map(objectToJSON);
};

export const postScholarships = async (scholarship: Scholarship) => {
  const userDoc = db.collection("users").doc(scholarship.sponsorId);
  const docRef = userDoc.collection("scholarships").doc();
  await docRef.set(scholarship);
  return scholarship;
};

export const updateScholarships = async (scholarship: Scholarship) => {
  const userDoc = db.collection("users").doc(scholarship.sponsorId);
  const docRef = userDoc.collection("scholarships").doc(scholarship.id);
  await docRef.update(scholarship);
  return scholarship;
};

export const deleteScholarships = async (scholarship: Scholarship) => {
  const userDoc = db.collection("users").doc(scholarship.sponsorId);
  const docRef = userDoc.collection("scholarships").doc(scholarship.id);
  await docRef.delete();
  return scholarship;
};

export const getScholarshipsById = async (id: string) => {
  const doc = await db.collection("scholarships").doc(id).get();
  console.log(doc);
  return doc.data();
}
