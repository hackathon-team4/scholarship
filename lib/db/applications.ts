import { auth, db, objectToJSON } from "../firebase";
import { Application } from "../models";

export const getPaginatedApplications = async (userID: string) => {
  const query = db
    .collection("users")
    .doc(userID)
    .collection("applications")
    .orderBy("createdAt", "desc");

  return (await query.get()).docs.map(objectToJSON);
};

export const getApplicationsByUser = async (userId: string) => {
  console.log(auth.currentUser);
  const query = db.collection("users").doc(userId).collection("applications");

  return (await query.get()).docs.map(objectToJSON);
};

export const getApplicationsCount = async (userID: string) => {
  const query = db
    .collection("users")
    .doc(userID)
    .collection("applications")
    .orderBy("createdAt", "desc");

  return (await query.get()).docs.length;
};

export const getApprovedApplicationsCount = async (userID: string) => {
  const query = db
    .collection("users")
    .doc(userID)
    .collection("applications")
    .where("status", "==", "approved")
    .orderBy("createdAt", "desc");

  return (await query.get()).docs.length;
};

export const postApplications = async (application: Application) => {
  const userDoc = db.collection("users").doc(application.studentId);
  const docRef = userDoc.collection("applications").doc();
  await docRef.set(application);
  return application;
};

// get the total value of all approved applications
export const getApprovedApplicationsTotal = async (userID: string) => {
  const query = db
    .collection("users")
    .doc(userID)
    .collection("applications")
    .where("status", "==", "approved")
    .orderBy("createdAt", "desc");

  const applications = (await query.get()).docs.map(objectToJSON);
  const total = applications.reduce((acc, curr) => acc + curr.amount, 0);
  return total;
};

export const updateApplications = async (application: Application) => {
  const userDoc = db.collection("users").doc(application.studentId);
  const docRef = userDoc.collection("applications").doc(application.id);
  await docRef.update(application);
  return application;
};

export const deleteApplications = async (application: Application) => {
  const userDoc = db.collection("users").doc(application.studentId);
  const docRef = userDoc.collection("applications").doc(application.id);
  await docRef.delete();
  return application;
};

export const getApplicationsById = async (id: string) => {
  const doc = await db.collection("applications").doc(id).get();
  return objectToJSON(doc);
};
