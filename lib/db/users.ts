import { db, objectToJSON, auth, googleAuthProvider } from "../firebase";
import { StudentProfile, SponsorProfile } from "../models";

// Auth exports
export const signInWithGoogle = async () => {
  const user = await auth.signInWithPopup(googleAuthProvider);
  return user;
};

export const signOut = async () => {
  await auth.signOut();
};

export const getUserProfile = async (uid) => {
  const user = await db.collection("users").doc(uid).get();
  return objectToJSON(user);
};

export const updateUserProfile = async (id, data) => {
  const user = await db.collection("users").doc(id).update(data);
  return objectToJSON(user);
};

export const createUserProfile = async (data) => {
  await db.collection("users").doc().set(data);
};

// Student Endpoints
export const getStudentProfile = async (userID: string) => {
  const doc = await db.collection("users").doc(userID).get();
  return objectToJSON(doc);
};

export const postStudentProfile = async (studentProfile: StudentProfile) => {
  const userDoc = db.collection("users").doc(studentProfile.id);
  await userDoc.set(studentProfile);
  return studentProfile;
};

export const updateStudentProfile = async (studentProfile: StudentProfile) => {
  const userDoc = db.collection("users").doc(studentProfile.id);
  await userDoc.update(studentProfile);
  return studentProfile;
};

export const deleteStudentProfile = async (studentProfile: StudentProfile) => {
  const userDoc = db.collection("users").doc(studentProfile.id);
  await userDoc.delete();
  return studentProfile;
};

// Sponsor Endpoints
export const postSponsorProfile = async (sponsorProfile: SponsorProfile) => {
  const userDoc = db.collection("users").doc(sponsorProfile.id);
  await userDoc.set(sponsorProfile);
  return sponsorProfile;
};

export const getSponsorProfile = async (userID: string) => {
  const doc = await db.collection("users").doc(userID).get();
  return objectToJSON(doc);
};

export const updateSponsorProfile = async (sponsorProfile: SponsorProfile) => {
  const userDoc = db.collection("users").doc(sponsorProfile.id);
  await userDoc.update(sponsorProfile);
  return sponsorProfile;
};

export const deleteSponsorProfile = async (sponsorProfile: SponsorProfile) => {
  const userDoc = db.collection("users").doc(sponsorProfile.id);
  await userDoc.delete();
  return sponsorProfile;
};
