export interface Scholarship {
  id: string;
  name: string;
  type: string;
  description: string;
  value: number;
  deadline: Date;
  image: string;
  createdAt: Date;
  updatedAt: Date;
  sponsorId: string;
}

export interface UserProfile {
  id: string;
  firstName: string;
  lastName: string;
  bio: string;
  gpa: number;
  major: string;
  school: string;
  role: string;
}

export interface StudentProfile {
  id?: string;
  name: string;
  school: string;
  grade: number;
  gpa: number;
  bio: string;
  studentId: string;
  email: string;
  photoUrl: string;
  createdAt: Date;
  updatedAt: Date;
}

export interface SponsorProfile {
  id?: string;
  name: string;
  bio: string;
  sponsorId: string;
  email: string;
  photoUrl: string;
  createdAt: Date;
  updatedAt: Date;
}

export interface Application {
  id?: string;
  studentId: string;
  status: string;
  scholarshipId: string;
  createdAt: Date;
  updatedAt: Date;
}
