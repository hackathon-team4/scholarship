import { auth, db } from "../lib/firebase";
import { useEffect, useState } from "react";
import { useAuthState } from "react-firebase-hooks/auth";

export function useUserData() {
  const [user] = useAuthState(auth);
  const [userProfile, setUserProfile] = useState(null);

  useEffect(() => {
    // turn off realtime subscription
    let unsubscribe;

    if (user) {
      const ref = db.collection('users').doc(user.uid);
      unsubscribe = ref.onSnapshot((doc) => {
        setUserProfile(doc.data()?.uid);
      });
    } else {
      setUserProfile(null);
    }

    return unsubscribe;
  }, [user]);

  return { user, userProfile };
}
