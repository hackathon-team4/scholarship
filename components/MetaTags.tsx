import Head from 'next/head';

export default function Metatags({
  title = 'Scholly',
  description = 'Use technology to foster connections and build a community of support for students.',
  image = '/public/graduate-grey.svg',
}) {
  return (
    <Head>
      <title>{title}</title>
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:site" content="scholarships" />
      <meta name="twitter:title" content={title} />
      <meta name="twitter:description" content={description} />
      <meta name="twitter:image" content={image} />
      <link rel="icon" href="/graduate-grey.svg" />

      <meta property="og:title" content={title} />
      <meta property="og:description" content={description} />
      <meta property="og:image" content={image} />
    </Head>
  );
}